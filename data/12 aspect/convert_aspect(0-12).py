print 'Convert string to number (aspect)'
file = open('input_aspect_901.txt','r')
output=open('input_value_901.txt','a+')
#s = '00020000'
for line in file:
	#print line
	line=str(line).strip()
	result= [pos for pos, char in enumerate(line) if char != '0']
	if len(result)>0:
		output.write(str(result[0]+1)+'\n')
		#print (result[0]+1)
	else:
		output.write('0'+'\n')
		#print '0'
	result=[]

output.close()
file.close()
print 'End!'