print 'Convert string to number (aspect)'
file = open('input_aspect.txt','r')
output=open('input_value.txt','a+')
# file = open('demo.txt','r')
# output=open('input_demo.txt','a+')

def getChar(s):
	for c in s:
		if c!='0':
			return c

for line in file:
	#print line
	line=str(line).strip()
	result= [pos for pos, char in enumerate(line) if char != '0']
	print result
	if len(result)>0:
		position=result[0]+1
		value = int(getChar(line))
		#print 'value=',value
		aspect=999
		if value==1:
			aspect = position
		elif value==2:
			aspect = value*6 + position
		elif value ==3:
			aspect = value*8 + position
		#print 'position: ',position
		#print 'aspect: ',aspect
		output.write(str(aspect)+'\n')
		#print (result[0]+1)
	else:
		output.write('0'+'\n')
		#print '0'
	result=[]

output.close()
file.close()
print 'End!'